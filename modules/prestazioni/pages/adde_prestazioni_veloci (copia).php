<html>
  <head>
    <title>Prestazioni veloci</title>
    <meta content="">
    <style></style>

<link href="/template/skin_sutti/css/stili_sutti_main.css" rel="stylesheet" type="text/css">

<script language="JavaScript" src="/plus/js/fxLoadPage.js" type="text/javascript"></script>
<script language="JavaScript" src="/plus/js/fxGenerali.js" type="text/javascript"></script>
<script language="JavaScript" src="/plus/js/fxContatti.js" type="text/javascript"></script>
<script language="JavaScript" src="/plus/js/fxImpegni.js" type="text/javascript"></script>
<script language="JavaScript" src="/plus/js/fxMail.js" type="text/javascript"></script>
<script language="JavaScript" src="/plus/js/fxPrestazioni.js" type="text/javascript"></script>
  </head>
  <body>



<select name="SelPrestazioni1" onchange="javascript:TarDaSelect(1)" id="SelPrest1"  class=""  onFocus="this.className='campo-focus-02'" onBlur="this.className='null'" > 
		 
<? 
	include("../../../config/config.php");
	include("../../../config/config_plus.php");
	$codice="0";
	$selettore=$CONF[titolo_selettore1];
	?> 
	<option selected="selected" value="<? echo $codice ?>"><? echo $selettore ?></option>
	<? 
	//Accede ai db
	$host=$CONF[db_host]; 
	$user=$CONF[db_user]; 
	$password= $CONF[db_pass];
	mysql_connect($host,$user,$password); 
	mysql_select_db($CONF[db_database]); 
	if ($CONF[usa_tariffario_knomos]==true) //Tariffario knomos
	{
		//Forma la query
		$sql = "SELECT id, tatid, tat_desc, selettore FROM INT_tariffe where selettore = 5 ORDER BY tatid" ;
		$esegui_query=mysql_query($sql) or die("Errore di autenticazione. "); 
		//Riempie il select
		while ($campo=mysql_fetch_array($esegui_query))
		{ 
		$codice=$campo['tatid'];
		?> 
		<option value="<? echo $codice ?>"><? echo $campo['tat_desc'] ?></option>
		<?
		}
	}
	else //Tariffario forense
	{
		//Forma la query
		$sql = "SELECT id, cod_tariffa, descrizione, selettore FROM INT_tariffe_STD where selettore = 1 ORDER BY id" ;
		$esegui_query=mysql_query($sql) or die("Errore di autenticazione. "); 
		//Riempie il select
		while ($campo=mysql_fetch_array($esegui_query))
		{ 
		$codice=$campo['cod_tariffa'];
		?> 
		<option value="<? echo $codice ?>"><? echo $campo['descrizione'] ?></option>
		<?
		}
	} 

?> 
</select> 


<select name="SelPrestazioni2" onchange="javascript:TarDaSelect(2)" id="SelPrest2"  class=""  onFocus="this.className='campo-focus-02'" onBlur="this.className='null'" > 
		<option selected="selected" value="0">Select2</option> 
<? 
	include("../../../config/config.php");
	$host=$CONF[db_host]; 
	$user=$CONF[db_user]; 
	$password= $CONF[db_pass];
	mysql_connect($host,$user,$password); 
	mysql_select_db($CONF[db_database]); 

	$sql = "SELECT id, tatid, tat_desc, selettore FROM INT_tariffe where selettore = 0 ORDER BY tatid" ;
	$esegui_query=mysql_query($sql) or die("Errore di autenticazione. "); 

	while ($campo=mysql_fetch_array($esegui_query))
	{ 
	$codice=$campo['tatid'];//.",".$campo['gma'].",".$campo['se_liberi'].",".$campo['se_todo'].",".$campo['se_evento'].",".$campo['cod_tipo_impegno'].",".$campo['priorita'].",".$campo['cod_prest'];
	?> 
	<option value="<? echo $codice ?>"><? echo $campo['tat_desc'] ?></option>
	<? 
	} 

?> 
</select> 







<table width="100%"  border="0" cellspacing="0">
	<tr onMouseOver="this.className='riga-focus-form'" onMouseOut="this.className='null'" >
	<td width="30%" class="" >
	Diritti nel processo di cognizione
	</td>
	<td width="70%" class=""  >
		<select name="SelDirCognizione" id="SelDirCogn" size="1" onchange="TarDirCogn()" class=""  onFocus="this.className='campo-focus-02'" onBlur="this.className='null'" >
		<option selected="selected" value="0">Selezionare l'onorario</option>
		<option value="XD002">1. Posizione e archivio</option>
		<option value="XD001">2. Per la disamina</option>
		<option value="XD002">3. Per la domanda introduttiva</option>
		<option value="XD001">4. Per la rinnovazione o riassunzione della domanda</option>
		<option value="XD001">5. Per la chiamata di un terzo in causa</option>
		<option value="XD001">6. Per l&acute;autentica di firma</option>
		<option value="XD001">7. Per esame procura notarile</option>
		<option value="XD001">8. Per il versamento contributo unificato</option>
		<option value="XD001">9. Per l&acute;iscrizione della causa a ruolo</option>
		<option value="XD001">10. Per la costituzione in giudizio</option>
		<option value="XD003">11. Per l&acute;esame degli scritti difensivi della controparte</option>
		<option value="XD003">12. Per l&acute;esame della documentazione prodotta da controparte</option>
		<option value="XD002">13. Per ogni scritto difensivo</option>
		<option value="XD003">14. Per ogni istanza, ricorso o reclamo</option>
		<option value="XD001">15. Per l&acute;esame di ogni dispositivo di provvedimento</option>
		<option value="XD003">16. Per l&acute;esame del testo integrale della sentenza od ordinanza</option>
		<option value="XD001">17. Per ogni dichiarazione resa nei casi espressamente previsti</option>
		<option value="XD001">18. Per la formazione del fascicolo e dell&acute;indice</option>
		<option value="XD003">19. Per la partecipazione ad udienza od operazioni del CTU</option>
		<option value="XD003">20. Per l&acute;assistenza della parte comparsa avanti al Giudice</option>
		<option value="XD002">21. Per le consultazioni col cliente</option>
		<option value="XD002">22. Per la corrispondenza informativa col cliente</option>
		<option value="XD001">23. Per la notificazione di ogni atto</option>
		<option value="XD001">24. Per l&acute;esame di ogni relata di notifica</option>
		<option value="XD007">25. Per la collaborazione prestata per la conciliazione</option>
		<option value="XD001">26. Per la intimazione ai testimoni</option>
		<option value="XD001">27. Per la designazione del CTP</option>
		<option value="XD003">28. Per l&acute;assistenza agli atti di istruzione probatoria</option>
		<option value="XD001">29. Per la richiesta di documenti e certificati</option>
		<option value="XD001">30. Per la richiesta di copia di atti</option>
		<option value="XD001">31. Per il deposito di atti e documenti</option>
		<option value="XD001">32. Per il ritiro del fascicolo di parte</option>
		<option value="XD001">33. Per sottoporre atti e documenti alla legalizzazione</option>
		<option value="XD001">34. Per ogni iscrizione nel FAL, GU o altre stampe</option>
		<option value="XD001">35. Per la proposizione di querela di falso</option>
		<option value="XD001">36. Per l&acute;esame delle prove testimoniali o interrogatorio</option>
		<option value="XD001">37. Per l&acute;esame di ogni relazione del consulente o documenti contabili</option>
		<option value="XD002">38. Per la precisazione delle conclusioni</option>
		<option value="XD002">39. Per l&acute;esame delle conclusioni di ogni controparte</option>
		<option value="XD003">40. Per la redazione della nota spese giudiziale</option>
		<option value="XD001">41. Per la richiesta di liquidazione al Consiglio dell&acute;Ordine</option>
		<option value="XD141">42. Per provvedere alla registrazione di ogni provvedimento</option>
		<option value="XD001">43. Per ogni deposito in Cancelleria</option>
		<option value="XD001">44. Per eseguire all'Uff. Registro i depositi previsti dalla legge</option>
		<option value="XD001">45. Per ogni accesso agli uffici</option>
		</select>

	</td>

	<tr onMouseOver="this.className='riga-focus-form'" onMouseOut="this.className='null'" >
	<td width="30%"  >
	Diritti nel processo di esecuzione:
	</td>
	<td width="70%" >
		<select name="SelDirEsecuzione" id="SelDirEsec" size="1" onchange="TarDirEsec()" onchange="TarDirCogn()" class=""  onFocus="this.className='campo-focus-02'" onBlur="this.className='null'"  >
		<option selected="selected" value="0">Selezionare l'onorario</option>
		<option value="XD001">46. Per la disamina di ogni titolo esecutivo</option>
		<option value="XD002">47. Per ogni precetto, pign. presso terzi o contro il terzo p.rio</option>
		<option value="XD001">48. Per la rich. di notif. titolo, precetto o pign. o rich. esec. mob.</option>
		<option value="XD002">49. Per l&acute;atto di pignoramento immobiliare o di navi o automobili</option>
		<option value="XD003">50. Per l&acute;esame del verbale di pignoramento mobiliare</option>
		<option value="XD007">51. Per la compilazione della nota di trascrizione od iscrizione</option>
		<option value="XD002">52. Per ogni ricorso al GE o di intimazione ad altri creditori</option>
		<option value="103">52. Per ogni istanza di fallimento o di insinuazionedel credito</option>
		<option value="XD003">53. Per la compilazione della nota di iscrizione o trascrizione/option>
		<option value="XD001">54. Per ogni richiesta di formalit&agrave; presso Pubblici Registri</option>
		<option value="XD003">55. Per l&acute;esame di ogni certificato ipotecario o catastale</option>
		<option value="XD003">56. Per la richiesta di ogni certificato ipotecario o catastale</option>
		<option value="XD003">57. Per le ispezioni ipotecarie (per ogni nota)</option>
		<option value="XD003">58 .Per le ispezioni catastali (per ogni nominativo)</option>
		<option value="XD001">59. Per ottenere la pubblicit&agrave; di avvisi</option>
		<option value="XD001">60. Per l&acute;esame di ciascuna domanda e dei titoli esecutivi</option>
		<option value="XD001">61. Per il deposito di somme</option>
		<option value="XD001">62. Per la domanda di vendita dei beni pignorati</option>
		<option value="XD003">63. Per ogni comparizione davanti al GE</option>
		<option value="XD001">64. Per la dichiarazione nella procedura di incanto</option>
		<option value="XD003">65. Per l'assistenza all&acute;incanto</option>
		<option value="XD003">66. Per le offerte all&acute;incanto</option>
		<option value="XD001">67. Per l&acute;offerta di acquisto dopo l&acute;incanto o durante l&acute;amm.ne giudiziaria</option>
		<option value="XD001">68. Per concorrere alla distribuzione del prezzo</option>
		<option value="XD002">69. Per la formazione del progetto di distribuzione amichevole del ricavato</option>
		<option value="XD002">70. Per l&acute;esame del progetto di distribuzione del ricavato dall&acute;esecuzione</option>
		<option value="XD002">71. Per la partecipazione alla discussione del prog. di distrib., per ogni ud.</option>
		<option value="XD002">72. Per l&acute;approvazione del progetto di distrib. del ricavato</option>
		<option value="XD003">73. Per l&acute;assistenza ad ogni adunanza dei credit. nel proc. esec. o concors.</option>
		<option value="#">74. Per ogni altra prestazione inerente il processo esecutivo</option>
		</select>

	</td>

	<tr onMouseOver="this.className='riga-focus-form'" onMouseOut="this.className='null'" >
	<td width="30%"  >
	Onorari civili:
	</td>
	<td width="70%" >
		<select name="SelOnCivili"  id="SelOnCiv" size="1" onchange="TarOnCiv()" class=""  onFocus="this.className='campo-focus-02'" onBlur="this.className='null'">
		<option selected="selected" value="0">Selezionare l'onorario</option>
		<option value="0"></option>
		<option value="0"><strong>I - Cause davanti al Giudice di Pace</strong></option>
		<option value="77">1. Per l&acute;intero giudizio - per le cause fino ad Euro 600,00</option>
		<option value="004">2. Studio della controversia</option>
		<option value="133">3. Consultazioni con il cliente</option>
		<option value="002">4. Ispezione dei luoghi della controversia - ric. dei documenti</option>
		<option value="14">5. Perparaz.  red. dell&acute;atto introduttivo o comp. di risposta</option>
		<option value="19">6. Assistenza a ciascuna udienza di trattazione</option>
		<option value="27">7. Assistenza ai mezzi istruttorio (per ogni mezzo)</option>
		<option value="13">8. Memorie depositate sino all&acute;ud. di precisazione delle conclusioni</option>
		<option value="108">9. Redazione delle difese (conclusionali o repliche)</option>
		<option value="111">10. Discussione in udienza</option>
		<option value="24">11. Opera prestata per la conciliazione ove avvenga in sede giudiziale</option>
		<option value="0"></option>
		<option value="0"><strong>I - Cause davanti al Tribunale od equiparati</strong></option>
		<option value="004">12. Studio della controversia</option>
		<option value="133">13. Consultazioni con il cliente</option>
		<option value="001">14. Ispezione dei luoghi della controversia - ric. dei documenti</option>
		<option value="14">15. Perparaz.  red. dell&acute;atto introduttivo o comp. di risposta</option>
		<option value="19">16. Assistenza a ciascuna udienza di trattazione</option>
		<option value="27">17. Assistenza ai mezzi istruttorio (per ogni mezzo)</option>
		<option value="13">18. Memorie depositate sino all&acute;ud. di precisazione delle conclusioni</option>
		<option value="108">19. Redazione delle difese (conclusionali o repliche)</option>
		<option value="111">20. Discussione in udienza</option>
		<option value="24">21. Opera prestata per la conciliazione ove avvenga in sede giudiziale</option>
		<option value="0"></option>
		<option value="0"><strong>II - Cause davanti al TAR, ricorsi straordinari o gerarchici</strong></option>
		<option value="004">22. Studio della controversia</option>
		<option value="133">23. Consultazioni con il cliente</option>
		<option value="002">24. Ricerca. dei documenti</option>
		<option value="14">25. Perparaz.  red. dell&acute;atto introduttivo o memoria di costituzione</option>
		<option value="7">26. Istanza di sospensione</option>
		<option value="14">27. Redazione dei motivi aggiunti</option>
		<option value="7">28. Atto di intervento</option>
		<option value="13">29. Assistenza ai mezzi istruttorio (per ogni mezzo)</option>
		<option value="108">30. Memorie difensive (per ciascuna)</option>
		<option value="111">31. Discussione in udienza</option>
		<option value="0"></option>
		<option value="0"><strong>IV - Cause davanti alla Corte di Appello ed alla Comm.Trib.Reg.</strong></option>
		<option value="004">32. Studio della controversia</option>
		<option value="133">33. Consultazioni con il cliente</option>
		<option value="002">34. Ispezione dei luoghi della controversia - ric. dei documenti</option>
		<option value="14">35. Perparaz.  red. dell&acute;atto introduttivo o comp. di risposta</option>
		<option value="19">36. Assistenza a ciascuna udienza di trattazione</option>
		<option value="27">37. Assistenza ai mezzi istruttorio (per ogni mezzo)</option>
		<option value="13">38. Memorie depositate sino all&acute;ud. di precisazione delle conclusioni</option>
		<option value="108">39. Redazione delle difese (conclusionali o repliche)</option>
		<option value="111">40. Discussione in udienza</option>
		<option value="24">41. Opera prestata per la conciliazione ove avvenga in sede giudiziale</option>
		<option value="0"></option>
		<option value="0"><strong>V - Cause davanti alla Corte di Cassazione ed altre Magistrature Sup.</strong></option>
		<option value="004">42. Studio della controversia</option>
		<option value="133">43. Consultazioni con il cliente</option>
		<option value="110">44. Redazione del ricorso, del controricorso, delle memorie</option>
		<option value="4141">45. Discussione</option>
		<option value="0"></option>
		<option value="0"><strong>IV - Cause davanti alla Corte Costituzionale ed equiparate</strong></option>
		<option value="#">46. Studio della controversia</option>
		<option value="#">47. Consultazioni con il cliente</option>
		<option value="#">48. Redazione del ricorso, del controricorso, delle memorie</option>
		<option value="#">49. Discussione</option>
		<option value="0"></option>
		<option value="0"><strong>VII - Procedimenti speciali, proc. esecutive e proc. tavolari</strong></option>
		<option value="80">50a. Proc. Spec. e concorsuali avanti ai Tribunali</option>
		<option value="#">50b. Proc. Spec. e concorsuali avanti le Corti di appello</option>
		<option value="81">51. Procedimenti di ingiunzione</option>
		<option value="46">52. Redazione del precetto</option>
		<option value="#">53. Iscrizione di ipoteca giudiziale</option>
		<option value="78">54. Procedure esecutive immobiliari</option>
		<option value="79">55. Procedure esecutive mobiliari e per affari tavolari</option>
		<option value="#">56. Procedure esecutive presso terzi o per consegna e rilascio</option>
		</select>

	</td>

	<tr onMouseOver="this.className='riga-focus-form'" onMouseOut="this.className='null'" >
	<td width="30%"  >
	Onorari penali:
	</td>
	<td width="70%" >
		<select name="SelOnPenali" id="SelOnPen" size="1" onchange="javascript:TarOnPenale()" class=""  onFocus="this.className='campo-focus-02'" onBlur="this.className='null'">
		<option selected="selected" value="0">Selezionare l'onorario</option>
		<option value="-P11">1) Corr. e sess.: 1.1. Informativa, anche telefonica</option>
		<option value="-P12">1) Corr. e sess.: 1.2. In studio col cliente</option>
		<option value="-P13A">1) Corr. e sess.: 1.3. In studio con colleghi o consulenti</option>
		<option value="-P13B">1) Corr. e sess.: 1.3. Fuori studio</option>
		<option value="-P20">2) Esame e studio</option>
		<option value="-P30A">3) INV.DIF.: Per colloquio, rich.dich.e assunz.di inform.</option>
		<option value="-P30B">3) INV.DIF.: Per rich.di docum.a PA/privati</option>
		<option value="-P30C">3) INV.DIF.: Per attivit&agrave; dif. relativa ad acc.tecnici</option>
		<option value="-P30C">3) INV.DIF.: Per la produz.di doc. ad Aut.Giudiziaria</option>
		<option value="-P40A">4) Indennit&agrave;: di accesso al carcere o ad uffici</option>
		<option value="-P40B">4) Indennit&agrave;: di accesso ai luoghi inerenti i fatti</option>
		<option value="-P40C">4) Indennit&agrave;: di attesa</option>
		<option value="-P50A">5) Partecipazione ed assistenza</option>
		<option value="-P50B">5) Partecip.ed assist.ad atti indagini preliminari</option>
		<option value="-P50C">5) Partecip.ed assist.ad attiv.di form./ric.della prova</option>
		<option value="-P61">6) Udienze: Per la partecipazione</option>
		<option value="-P62">6) Udienze: Per la partecipaz.ad attiv.difensive</option>
		<option value="-P63">6) Udienze: Per la discussione orale</option>
		<option value="-P71">7) Redaz.scritti: 7.1. Esposti, denunzie, querele</option>
		<option value="-P72A">7) Redaz.scritti: 7.2. Istanze, opp.ni, dichiaraz., richieste</option>
		<option value="-P72B">7) Redaz.scritti: 7.2. Ricorso immediato al GdP</option>
		<option value="-P73">7) Redaz.scritti: 7.3. Liste testimoniali</option>
		<option value="-P74">7) Redaz.scritti: 7.4. Citazioni e notifiche</option>
		<option value="-P75">7) Redaz.scritti: 7.5. Impugnazioni</option>
		<option value="-P76">7) Redaz.scritti: 7.6. Memorie</option>
		<option value="-P77">7) Redaz.scritti: 7.7. Pareri che esauriscono attivit&agrave;</option>
		</select>

	</td>

	</table>

</body>
</html>
