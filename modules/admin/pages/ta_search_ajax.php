<?
$time = microtime(true);
include("../../../framework/framework.php");
//http://localhost:82/modules/admin/pages/ta_search_ajax.php?simple_ins=1&form_id=listpratiche&codice=105&formname=listprestaz&fieldname=codice[text]
$module = "admin";

ob_start();

$thissearch = load_fwobject("search", $module, 6);

$search = $thissearch["search"];
//Init della ricerca
foreach ($search["fields"] as $key => $field) {
	list($type, $options) = explode("||", $field["content"]);

	unset($target);
	foreach (explode("||", $field["search_field"]) as $trg) {
		$target[] = explode("-", $trg);
	}

	unset($cnt);

	$cntt = array();

	foreach ($target as $st) {

		if (!isset($SQL_SEARCH[$st[0]])) {

			if (!isset($search["tablesql"][$st[0]])) {
				$SQL_SEARCH[$st[0]] = "SELECT * FROM " . $st[0] . " WHERE %[PERM]% ";
			} else $SQL_SEARCH[$st[0]] = $search["tablesql"][$st[0]];
		}

		if ($cntt[$st[0]] == 0) {
			$SQL_SEARCH[$st[0]] .= " AND ( ";
		} else $SQL_SEARCH[$st[0]] .= " OR (";

		$cntt[$st[0]]++;

		// Create SQL Query according to search type
		if (is_array($_GET[$key]) && isset($_GET[$key]["realval"]) && count($_GET[$key]["realval"]) > 1) {
			$tcnt = count($_GET[$key]["realval"]);
			$linarray = 1;
		} elseif (is_array($_GET[$key]) && count($_GET[$key]) > 1  && !isset($_GET[$key]["realval"])) {
			$tcnt = count($_GET[$key]);
			$linarray = 1;
		} else {
			$tcnt = 1;
			$linarray = 0;
		}

		$opened = 0;
		for ($cnt = 0; $cnt < $tcnt; $cnt++) {
			unset($this_round);
			if (is_array($_GET[$key]) && is_array($_GET[$key]["realval"])) {
				$curval = $_GET[$key]["realval"][$cnt];
			} elseif (is_array($_GET[$key])) {
				$curval = $_GET[$key][$cnt];
			} else {
				$curval = $_GET[$key];
			}

			if (strlen($curval) > 0) {
				if ($cnt > 0) $SQL_SEARCH[$st[0]] .= " OR ";
				if ($opened != 1 && $linarray == 1) {
					$SQL_SEARCH[$st[0]] .= " ( ";
					$opened = 1;
				}

				switch ($type) {
					case "text":
						$SQL_SEARCH[$st[0]] .= $st[1] . "='" . $curval . "' ";
						if (strlen($curval) > 0) $SEARCH_FIELDS[$st[0]][$st[1]][] = $curval;
						break;
					case "text_like":
						$SQL_SEARCH[$st[0]] .= $st[1] . " LIKE '%" . $curval . "%' ";
						if (strlen($curval) > 0) $SEARCH_FIELDS[$st[0]][$st[1]][] = $curval;
						break;
					case "text_start":
						$SQL_SEARCH[$st[0]] .= $st[1] . " LIKE '" . $curval . "%' ";
						if (strlen($curval) > 0) $SEARCH_FIELDS[$st[0]][$st[1]][] = $curval;
						break;
					case "text_end":
						$SQL_SEARCH[$st[0]] .= $st[1] . " LIKE '%" . $curval . "' ";
						if (strlen($curval) > 0) $SEARCH_FIELDS[$st[0]][$st[1]][] = $curval;
						break;
					case "text_word":
						$SQL_SEARCH[$st[0]] .= $st[1] . " LIKE '% " . $curval . " %' ";
						if (strlen($curval) > 0) $SEARCH_FIELDS[$st[0]][$st[1]][] = $curval;
						break;
					case "text_ext":
						$SQL_SEARCH[$st[0]] .= ext_search($st[1], $curval);
						if (strlen($curval) > 0) $SEARCH_FIELDS[$st[0]][$st[1]][] = $curval;
						break;
					case "user_perm":
						$SQL_SEARCH[$st[0]] .= $st[1] . " LIKE '%U" . $curval . "=33333%'";
						if (strlen($curval) > 0) $SEARCH_FIELDS[$st[0]][$st[1]][] = " ";
						break;
					case "calendar_owner":
						$SQL_SEARCH[$st[0]] .= $st[1] . " LIKE '" . $curval . ",,%' OR " . $st[1] . " LIKE '%,," . $curval . "' OR " . $st[1] . " LIKE '%,," . $curval . ",,%' OR " . $st[1] . " LIKE '" . $curval . "'";
						if (strlen($curval) > 0) $SEARCH_FIELDS[$st[0]][$st[1]][] = " ";
						break;

					case "num_less":
						$SQL_SEARCH[$st[0]] .= $st[1] . " <= '" . $curval . "' ";
						if (strlen($curval) > 0) $SEARCH_FIELDS[$st[0]][$st[1]][] = $curval;
						break;
					case "num_more":
						$SQL_SEARCH[$st[0]] .= $st[1] . " >= '" . $curval . "' ";
						if (strlen($curval) > 0) $SEARCH_FIELDS[$st[0]][$st[1]][] = $curval;
						break;
					case "num_equal":
						$SQL_SEARCH[$st[0]] .= $st[1] . " = '" . $curval . "' ";
						if (strlen($curval) > 0) $SEARCH_FIELDS[$st[0]][$st[1]][] = $curval;
						break;
				}
				$done_seart[$st[0]]++;
			}
		}
		if ($linarray == 1 && $opened == 1) $SQL_SEARCH[$st[0]] .= " ) ";

		$SQL_SEARCH[$st[0]] .= ')';
		$SQL_SEARCH[$st[0]] = str_replace("AND ( )", "", $SQL_SEARCH[$st[0]]);
	}

	/*		//Close sql )
		
		foreach($SQL_SEARCH as $table => $sql)
		{
				$SQL_SEARCH[$table].=')';
				$SQL_SEARCH[$table]=str_replace("AND ( )","",$SQL_SEARCH[$table]);
		}
	*/
}

foreach ($search["tables"] as $table => $options) {
	list($TABLES[$table]["list_var"], $TABLES[$table]["title"], $TABLES[$table]["options"], $TABLES[$table]["permission"]) = explode("||", $options);
}


//CHECK FOR EMPTY SEARCH
if ($search["allow_empty"] != 1) {
	foreach ($SQL_SEARCH as $table => $sql) {
		if ($done_seart[$table] == 0) {
			unset($SQL_SEARCH[$table]);
		}
	}
}

//Check for result number
foreach ($SQL_SEARCH as $table => $sql) {
	list($perm_type, $perm_parent) = explode(";;", $TABLES[$table]["permission"]);

	if ($perm_type == 0) {
		$sql = str_replace("%[PERM]%", "1=1", $sql);
	}

	$rs = @$DB->Execute(perm_sql_read($sql, $table));
	$res = $rs->RecordCount();
	$totr += $res;
	$RESULTNUM[$table] = $res;
}

$RESULTNUM["total"] = $totr;

$result=(object)null;

if($RESULTNUM["total"]==1)
	$result = $rs->FetchRow();	

print json_encode($result);

$PAGE["PAGE_CONTENT"] = ob_get_contents();
ob_end_clean();
header('Content-Type: application/json');
echo $PAGE["PAGE_CONTENT"];
//echo ( microtime( true ) - $time ) . ' seconds';
