<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>%[TXT_TITLE]%</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="%[CSS_GPATH]%stili_sutti_01.css" rel="stylesheet"
	type="text/css">
<link href="%[CSS_GPATH]%stili_sutti_main.css" rel="stylesheet"
	type="text/css">

<script language="JavaScript" type="text/javascript"
	src="%[JS_GPATH]%include_01.js"></script>
</head>
<body>

<div id="main">%[PAGE_CONTENT]%</div>
</body>
</html>
